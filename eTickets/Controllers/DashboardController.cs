﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using eTickets.Data.Services;
using eTickets.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eTickets.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IDashboardService _dashboardService;

        private readonly IOrdersService _ordersService;

        public DashboardController(IDashboardService service, IOrdersService ordersService)
        {
            _dashboardService = service;
            _ordersService = ordersService;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var dashboard = new DashboardVM()
            {
                Orders = await _dashboardService.GetOrderAmount(),
                Users = await _dashboardService.GetUserAmount(),
                Cinemas = await _dashboardService.GetCinemaAmount(),
                Available_Movies = await _dashboardService.GetAvailableMovieAmount()
            };

            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userRole = User.FindFirstValue(ClaimTypes.Role);

            var orders = await _ordersService.GetOrdersByUserIdAndRoleAsync(userId, userRole);

            dynamic model = new ExpandoObject();
            model.dashboard = dashboard;
            model.orders = orders;

            return View(model);
        }

        public async Task<IActionResult> GetSales()
        {
            var salesToday = await _dashboardService.GetSalesToday();
            var salesYesterday = await _dashboardService.GetSalesYesterday();
            var salesTwoDaysAgo = await _dashboardService.GetSalesTwoDaysAgo();

            var totalSalesToday = await _dashboardService.GetTotalSalesToday();
            var totalSalesYesterday = totalSalesToday - salesToday;
            var totalSalesTwoDaysAgo = totalSalesToday - salesToday - salesYesterday;

            return Json(new { salesToday, salesYesterday, salesTwoDaysAgo, totalSalesToday, totalSalesYesterday, totalSalesTwoDaysAgo });
        }
    }
}
