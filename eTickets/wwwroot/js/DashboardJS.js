﻿var salesToday;
var salesYesterday;
var salesTwoDaysAgo;

var totalSalesToday;
var totalSalesYesterday;
var totalSalesTwoDaysAgo;

$(document).ready(function () {
    $.ajax({
        url: "/Dashboard/GetSales",
        type: "GET",
        dataType: "json",
        success: function (data) {
            salesToday = data.salesToday;
            salesYesterday = data.salesYesterday;
            salesTwoDaysAgo = data.salesTwoDaysAgo;

            totalSalesToday = data.totalSalesToday;
            totalSalesYesterday = data.totalSalesYesterday;
            totalSalesTwoDaysAgo = data.totalSalesTwoDaysAgo;

            //Sales per day graph (in three days)
            const salesPerDayChart = document.getElementById('salesPerDayChart');
            var timeFrom = (X) => {
                var dates = [];
                for (let I = 0; I < Math.abs(X); I++) {
                    dates.push(new Date(new Date().getTime() - ((X >= 0 ? I : (I - I - I)) * 24 * 60 * 60 * 1000)).toDateString());
                }
                return dates.reverse();
            }
            const labels = timeFrom(+3);
            const lineChartData = {
                labels: labels,
                datasets: [{
                    label: 'Sales Per Day',
                    data: [parseFloat(salesTwoDaysAgo), parseFloat(salesYesterday), parseFloat(salesToday)],
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1
                }]
            };
            const lineChart = new Chart(salesPerDayChart, {
                type: 'line',
                data: lineChartData,
                options: {
                    scales: {
                        y: {
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function (value, index, ticks) {
                                    return '$' + value;
                                }
                            }
                        }
                    }
                }
            });

            //Total Sales per day graph (in three days)
            const totalSalesPerDayChart = document.getElementById('totalSalesPerDayChart');

            const barChartData = {
                labels: labels,
                datasets: [{
                    label: 'Total Sales Per Day',
                    data: [parseFloat(totalSalesTwoDaysAgo), parseFloat(totalSalesYesterday), parseFloat(totalSalesToday)],
                    backgroundColor: "#FF0000",
                }]
            };


            const barChart = new Chart(totalSalesPerDayChart, {
                type: 'bar',
                data: barChartData,
                options: {
                    scales: {
                        y: {
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function (value, index, ticks) {
                                    return '$' + value;
                                }
                            }
                        }
                    }
                }
            });
        }
    });
});




