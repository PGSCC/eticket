﻿using System;
using eTickets.Models;

namespace eTickets.Data.ViewModels
{
    public class DashboardVM : Order
    {
        public int Orders { get; set; }

        public int Users { get; set; }

        public int Cinemas { get; set; }

        public int Available_Movies { get; set; }
    }
}
