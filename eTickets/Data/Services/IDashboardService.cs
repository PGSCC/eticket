﻿using System;
using System.Threading.Tasks;

namespace eTickets.Data.Services
{
    public interface IDashboardService
    {
        Task<int> GetOrderAmount();

        Task<int> GetUserAmount();

        Task<int> GetCinemaAmount();

        Task<int> GetAvailableMovieAmount();

        Task<double> GetSalesToday();

        Task<double> GetSalesYesterday();

        Task<double> GetSalesTwoDaysAgo();

        Task<double> GetTotalSalesToday();
    }
}
