﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace eTickets.Data.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly AppDbContext _context;

        public DashboardService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<int> GetAvailableMovieAmount() => await _context.Movies.Where(n => n.StartDate < DateTime.Now && n.EndDate > DateTime.Now).CountAsync();

        public async Task<int> GetCinemaAmount() => await _context.Cinemas.CountAsync();

        public async Task<int> GetOrderAmount() => await _context.Orders.CountAsync();

        public async Task<double> GetSalesToday() => await _context.OrderItems.Where(n => n.OrderTime == DateTime.Today).Select(n => n.Price * n.Amount).SumAsync();

        public async Task<double> GetSalesTwoDaysAgo() => await _context.OrderItems.Where(n => n.OrderTime == DateTime.Today.AddDays(-2)).Select(n => n.Price * n.Amount).SumAsync();

        public async Task<double> GetSalesYesterday() => await _context.OrderItems.Where(n => n.OrderTime == DateTime.Today.AddDays(-1)).Select(n => n.Price * n.Amount).SumAsync();

        public async Task<double> GetTotalSalesToday() => await _context.OrderItems.Select(n => n.Price * n.Amount).SumAsync();

        public async Task<int> GetUserAmount() => await _context.Users.CountAsync();

    }
}
